/*

********************************************************
        NOM du projet : Arcade
********************************************************


Auteur(s) : AMADOU BARRY
Date de la derniere revision : 19/09/22
Version : 1.0
Auteur(s) revision :
Date de revision :

Fonctionnalite :
                                Ce projet implemente en POO une borne arcade
                                >> Permet d'ajouter un jeux � l'arcade de mani�re flexible
                                >> Sauvegardes les donnees du joueur et des jeux � la fermeture de l'arcade
                                >> Permet de charger l'historiques des parties jouer par un utilisateur

*/


#include "utile.h"
#include "arcade.h"



int main()
{

    /*
    Pour ajouter un nouveau jeu :
        >> Creer une classe propre au jeu par heritage de la classe Jeux
        >> implementer une methode boucleprinciapale pour le jeu
        >> Modifier le constructeur de l'arcade pour inclure ce nouveau jeu � la  creation de l'arcade
        >> ajouter le nom du jeux dans le vecteur listJeux
    */
    vector<string> listjeux = {"pendu", "mastermind", "juste_prix"}; //  vecteur des jeux implementer
    Arcade arcade("version 1.0",listjeux);


    arcade.connexion();     // connexion � la borne d'arcade avec un pseudo unique

    arcade.loaddataJeux(""); // chargement des tableaux de meilleurs pour chaque jeux

    bool stop = false;
    while(!stop){

        sleep(2);
        system("cls");
        arcade.setcolor();


        arcade.MenuPrincipale();    // affichage du menu principal

        int choix_menu(0);
        cin >> choix_menu;

        switch (choix_menu)
        {
            case 1 : {
                cout << "Liste des Jeux : " << endl ;
                int i(0);
                for (i = 0 ; i<(int)arcade.getJeux().size();i++){
                    cout <<"\t\t"<<i+1<<" ."<<arcade.getJeux()[i]->getnom()<<endl;
                }
                int choix_jeux(0);
                cin >> choix_jeux;
                system("cls");
                arcade.getJeux()[choix_jeux-1]->menu(arcade.getJoueur()); // lancement du jeu choix
            }
            break;

            case 2 : {

                // info joueur
                cout << "Pseudo : " << arcade.getJoueur()->getPseudo() << endl;
                cout << "Option : ";
                map <string,string> :: iterator it;
                map <string,string> mapoption  = arcade.getJoueur()->getOptions();
                cout << "{";
                for (it=mapoption.begin();it!=mapoption.end();it++)
                {
                    std::cout << it->first    // string (key)
                              << ':'
                              << it->second   // string's value
                              << " ";
                }
                cout << "}"<<endl;

                cout << "Scores Jeux : " << endl;
                map <string,vector<int>> :: iterator it2;
                map <string,vector<int>> scores  = arcade.getJoueur()->getScores();
                for (it2=scores.begin();it2!=scores.end();it2++)
                {

                    std::cout << it2->first    // string (key)
                              << ':'<< endl;
                    for (auto j : it2->second) cout  << "\t" << j<< endl;   // string's value

                }

            }
            break ;

            case 3 : {
                // credit
                cout << " Projet developper dans le cadre de la formation AJC - DEV C++"
                     << " Auteur : BARRY AMADOU DIOULDE"<< endl;

            }
            break ;

            case 4 : {

                // options couleurs
                cout << "Couleur [available : red - green ] : ";
                string color ;
                cin >> color;
                arcade.getJoueur()->addOption("color",color);

            }
            break ;

            default : {
                stop = true;
            }
            break;

        }

    }

    // sauvegarde des donnnees
    arcade.savedata("");

    return 0;
}
