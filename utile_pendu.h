#ifndef UTILE_PENDU_H_INCLUDED
#define UTILE_PENDU_H_INCLUDED

#include <iostream>
#include <string.h>
#include <vector>
#include <time.h>

using namespace std;
void vider_buffer();
int random_int(int MAX, int MIN);
string choisir_option(const char* texte,vector<string> tableau_option,int TAILLE,int* P_choix);
int devoile_mot(string mot_mystere,string* mot_affiche, char lettre_choisie);




#endif // UTILE_PENDU_H_INCLUDED
