#include "pendu.h"
#include "utile_pendu.h"


int Pendu::bouclePrincipale(shared_ptr<Joueur>joueur)
{
     // constantes

    const int MAX_MOT = 3;
    const int Max_THEME = 3;


    int choix_theme ;
    char lettre_choisie='@';

    int taille_mot;

    int victoire = 0 ;
    int flag_trouve = 0;
    int rejoue = 0 ;

    // variable pour rejouer
    int victoire_joueur=0;
    int nb_match=0;
    int return_scanf;

    int chance;



    vector<string> const tableau_theme= {"animaux","informatique","jeux"};
    vector<string> const oui_non = {"non","oui"};

    vector<vector<string>> tableau_mot_myster = {{"chien","chat","lapin"},
    {"logiciel","informatique","programmation"},
    {"pendu","mastermind","JustePrix"}};

    const int max_chance = 10;
    int max_score=max_chance;

    string mot_mystere;


    // intialisation du generateur aleatoire
    srand(time(NULL));

    // boucle principal -- rejouer


    do {

    // Menu principal

    string theme = choisir_option("choisir un th�me :",tableau_theme,Max_THEME,&choix_theme);
    cout << "vous avez choisi le theme : " << theme << endl;

      // generer le mot mystere
    int k = random_int(MAX_MOT,0);

    mot_mystere = tableau_mot_myster[choix_theme][k];

    //cout << "spoil !! mot mystere : " << mot_mystere << endl;


      // initialiser le mot � afficher

    string mot_affiche(mot_mystere.size(),'*');




        // second boucle -- nb_iteration
    chance = max_chance;

    while(chance>0) {

                 // message d'acceuil
                system("cls");
                cout << "-------- Pendu ---------------" << endl ;
                cout << "mot mystere : " << mot_affiche << "\t " << mot_affiche.size() << " lettres" << endl;
                cout << "il vous reste " <<  chance << " essaies" << endl ;

                // demander une lettre au joueur

                do {
                        cout << "entrer une lettre (EN MINUSCULE) :" << endl ;
                        cout << ">" ; cin >> lettre_choisie;

                        vider_buffer();// vider le buffer

                        // on recupere uniquement les caractereres ascii entre a-z (en minuscule)
                        if(lettre_choisie>122||lettre_choisie<97){
                        cout << "---- OUPS !! SAISIE INCORRECT !!!!" << endl;
                        }

                }while(lettre_choisie>122||lettre_choisie<97);

                cout << "lettre choisie : " << lettre_choisie << endl ;

                // rechercher la lettre dans le mot mystere

                flag_trouve = devoile_mot(mot_mystere,&mot_affiche,lettre_choisie);

                cout << "mot mystere : " << mot_affiche << "\t " << mot_affiche.size() << " lettres" << endl;


                 // condition si victoire
                    // compare mot_ mystere et mot afficher
                if (!mot_affiche.compare(mot_mystere)){
                    cout << "-----> BRAVO, VOUS AVEZ GAGNE !!!!";
                    victoire_joueur++;
                    break;
                }

                // sinon si pas vicoire

                else {
                    if (!flag_trouve){

                        cout << lettre_choisie << " n'est pas dans " << mot_affiche << "\t Vous perdez 1 point" << endl ;
                        chance --;
                    }

                    else {
                        cout << lettre_choisie << " est bien dans " << mot_affiche << endl;
                    }

                    if (!chance){
                        cout << "----> VOUS AVEZ PERDU !!!! " << endl ;

                    }




                }

        }


        nb_match++;


    choisir_option("voulez vous rejoue : ",oui_non,2,&rejoue);

    max_score = chance>max_score ? chance : max_score ;


   }while(rejoue);



    cout << "Tu as gagn� " <<  victoire_joueur<< " matchs sur " << nb_match << " contre l'ordinateur " << endl;

    cout << "Max score : " <<  max_score << endl;



    cout << "---Fin de partie------" << endl;

    return max_score;

//    cout << " boucle principale pendu" << endl;
//    return 0;
}

void Pendu::regle(){
    cout << u8"les regles du pendu" << endl;
    cout << u8"un mot mystere un tirer aleatoirement dans une liste de mots du th�me choisi par le joueur"<< endl;
    cout << u8"le joueur saisie une lettre"<<endl;
    cout << u8"si la lettre existe dans le mot mystere, le programme devoile la position de la lettre dans le mot"<<endl;
    cout << u8"si la lettre n'existe pas dans le mot mystere, le joueur perd une chance"<< endl;
    cout << u8"le joueur � un nombre limite d'essaie" << endl;
    cout << u8"le score du joueur correspond au nombre d'essaie restant "<< endl;
}
