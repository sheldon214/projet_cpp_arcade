#ifndef JOUEUR_H_INCLUDED
#define JOUEUR_H_INCLUDED

#include "utile.h"

class Joueur{

 private :
     string pseudo;
     map <string,string> options;
     map <string,vector<int>> scores;

 public :
        string getPseudo(){
        return this->pseudo;}


        Joueur(string nom){
        this->pseudo = nom;
        this->options["color"]= "red";
        }
        map<string,string> getOptions(){return this->options;}
        map<string,vector<int>> getScores(){return this->scores;}

        void addScore(string nom_jeux,int score);
        void addOption(string option_name,string option_value){this->options[option_name]=option_value;}
        void setPseudo(string nom){this->pseudo=nom;}




};

#endif // JOUEUR_H_INCLUDED
