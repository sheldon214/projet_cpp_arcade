#ifndef JEUX_H_INCLUDED
#define JEUX_H_INCLUDED


#include "utile.h"
#include "joueur.h"


class Jeux{

private :
        string nomjeu;
        vector<int> tabscores;
        vector<string> tabpseudo;
        int max_scores = 5;

public :

    Jeux(string nom);

    string getnom(){return this->nomjeu;}
    vector<int> getTabScores(){return this->tabscores;}
    vector<string> getTabPseudo(){return this->tabpseudo;}
    void set_scores(int, int);
    void set_pseudo(int,string);

    virtual int bouclePrincipale(shared_ptr <Joueur> joueur){};
    bool AddScore(string pseudo, int nv_score);
    virtual void regle(){};
    void printab();
    void menu(shared_ptr<Joueur> joueur);

};


#endif // JEUX_H_INCLUDED
