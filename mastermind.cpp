#include "mastermind.h"

int Mastermind::bouclePrincipale(shared_ptr<Joueur>joueur)
{
   srand(time(NULL));

    // declaration des variables

    // variable gestion des erreurs de saisie utilisateur
	int return_scanf = 0;
	int c;
	int valide_carac;

    int rejoue = 0;

    int nb_tentative =1;
    const int Max_tentative = 10;

    int mal_palce = 0, b_place=0;                           // compteur des bonnes reponses

    // TABLEAUX DE GESTION DU JEU
    char code[4] = {'B','V','V','B'};               // LE CODE A TROUVE
    char saisie_utilisateur[4] = {'*','*','*','*'};     // LA SAISIE UTILISATEUR
    const int taille_code = 4;                          // TAILLE DU CODE
    char code_v[4] = {'R','V','J','O'};             // TABLEAU POUR METTRE A JOUR LES COULEURS PRESENTES BIEN ET MAL PLACEES

    // variable pour la generation aleatoire du code
    char code_valide[5] = {'R','B','V','J','O'};                // liste de couleur valide
    const int VALEUR_MIN = 0, VALEUR_MAX = taille_code-1;

    // variable pour rejouer
    int victoire_joueur=0;
    int nb_match=0;

    int score = Max_tentative ;

    do{



    printf("-------------Bienvenue dans le mastermind-------------------------\n");

  //   generation du code aleatoire

    for (int i = 0; i<taille_code; i++){

        code[i] = code_valide[(rand() % VALEUR_MAX) + VALEUR_MIN];

    }




   // Boucle principale
    while(nb_tentative<=Max_tentative){

    // spoil du code mystere (� commenter)
//    printf("(SPOIL!!) le code est : { ");
//    for (int i =0 ; i<taille_code;i++){
//        printf("%c ",code[i]);
//
//    }
//    printf("}\n");

    // initialisation des compteurs
    mal_palce = 0;
    b_place = 0;

    // initialisation de la copie du code modifier
    for (int i =0 ; i<taille_code;i++){
        code_v[i] = code[i] ;
    }

        // boucle qui verifie que la saisie de l'utilisateur est valide
        do {

                printf("Donnez un code de 4 couleurs differentes et sans espaces parmis {'R', 'V', 'B', 'J', 'O'} : ");
                return_scanf = scanf("%4c",saisie_utilisateur);

                while ( (c = getchar()) != '\n' && c != EOF ) { }   // vider le buffer



                for (int i =0;i<taille_code;i++){
                    valide_carac = 0;
                    for (int j = 0; j<taille_code;j++){
                        if (saisie_utilisateur[i]==code_valide[j]){
                            valide_carac=1;
                        }
                    }

                    if(!valide_carac){
                        break;
                    }

                }

       }while(!return_scanf||!valide_carac);


        // calcul des couleurs bien placee dans le code
        for (int i =0;i<taille_code;i++) {
           // printf("code[%d] = %c\tsaisie[%d] = %c\n",i,code[i],i,saisie_utilisateur[i]);
            if (code_v[i]==saisie_utilisateur[i]){
                code_v[i]='@';
                b_place++;
//                printf("bien placee\n");
//                printf("code[%d] = %c",i,code[i]);
//                printf("\tcode_v[%d] = %c",i,code_v[i]);
//                printf("\tsaisie[%d] = %c\n",i,saisie_utilisateur[i]);

            }
        }

        // calcul des couleurs mal placee dans le code
        for (int i =0;i<taille_code;i++){

               if(code_v[i]!='@') {

                for (int j=0;j<taille_code;j++){
                    if(code_v[j]==saisie_utilisateur[i]){
                        mal_palce++;
                        code_v[j] = '*';
//                        printf("mal placee\n");
//                        printf("code[%d] = %c",j,code[j]);
//                        printf("\tcode_v[%d] = %c",j,code_v[j]);
//                        printf("\tsaisie[%d] = %c\n",i,saisie_utilisateur[i]);
                        break;
                    }

                }
            }
        }

        if (b_place == taille_code){
            printf("BRAVOO, c'est gagne !!!!\n");
            victoire_joueur++;
            break;
        }
        else {

            printf("couleur mal placees : %d\n",mal_palce);
            printf("couleur bien placees : %d\n",b_place);
            printf("il vous reste %d essaies\n",Max_tentative-nb_tentative);
            nb_tentative++;
        }
    }


    if (nb_tentative+1==Max_tentative){
        printf("PERDU!!!!!\n");
    }

    printf("le code est : { ");
    for (int i =0 ; i<taille_code;i++){
        printf("%c ",code[i]);
    }
    printf(" } \n");

    nb_match++;

    do{

    printf("voulez vous rejoue (1.oui, 0.non) : ");
    return_scanf = scanf("%1d",&rejoue);
    while ( (c = getchar()) != '\n' && c != EOF ) { }   // vider le buffer
    }while(!return_scanf||rejoue>1||rejoue<0);


    score = (Max_tentative-nb_tentative) > score ? (Max_tentative-nb_tentative) : score;


    }while(rejoue);


    printf("Tu as gagn� %d matchs sur %d contre l'ordinateur \n",victoire_joueur,nb_match);
    printf("---Fin de partie------\n");

    return score;
}


void Mastermind::regle()
{
    cout << "les regles du mastemind" << endl;
    cout << "Un code secret a 4 caractere est tirer aleatoirement en utilisant les caracteree R, V, J, O, B"<< endl;
    cout << "le joueur entre un code avec 4 caracterer" << endl;
    cout << "le programme indique les caractere bien placee et celle mal placee"<< endl;
    cout << "le joueur doit trouver le code secret en un nombre limite d'essaie"<< endl;
    cout << "le score correpond au nombre d'essaie restant "<< endl;
}
