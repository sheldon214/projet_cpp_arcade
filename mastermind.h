#ifndef MASTERMIND_H_INCLUDED
#define MASTERMIND_H_INCLUDED

#include "utile.h"
#include "jeux.h"
#include "time.h"

class Mastermind:public Jeux {

    private  :

    public :

        int bouclePrincipale(shared_ptr<Joueur>joueur);
        Mastermind(string nom):Jeux(nom){}
        void regle();

};

#endif // MASTERMIND_H_INCLUDED
