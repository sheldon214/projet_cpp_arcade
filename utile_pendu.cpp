#include "utile_pendu.h"


void vider_buffer(){
    /**

        Fonction qui vide le buffer apres un appel de scanf ou fgets

    **/
    char c;

    while ( (c = getchar()) != '\n' && c != -1 );  // vider le buffer

}

int random_int(int MAX, int MIN){

    /**

        Fonction qui genere un nombre entier N tel que MIN <= N < Max

    **/

    return ((rand() % MAX) + MIN);

}

string choisir_option(const char* texte,vector<string> tableau_option,int TAILLE,int* P_choix){

    /**
            fonction qui permet � l'utilisateur de choisir une option dans une liste de string

            entree :
                texte : texte a afficher avant la demande de saisie
                tableau_option : listes des options valide (ranger par ordre de choix de 0 � TAILLe -1
                Taille : nombre d'element dans tableau_option
                P_choix : adresse de la variable utiliser pour sauvegarder le choix de l'utilisateur

             sortie :
                tableau_option[choix] : valeur de l'option choisie par l'utilisateur


        **/
    int choix {-1};

     do {
        cout << texte << endl;

        for (int i=0; i<TAILLE;i++){

       cout << "\t\t" << i << "." << tableau_option[i] << endl;

        }

     cout << ">" ; cin >> choix ;

     vider_buffer();

        if (choix>=TAILLE||choix<0){
            cout << "---- OUPS !! SAISIE INCORRECT !!!!" << endl;
        }

    }while(choix>=TAILLE||choix<0);

    *P_choix = choix;

    return tableau_option[choix];
}


int devoile_mot(string mot_mystere,string* mot_affiche, char lettre_choisie){

    /**

        fonction qui devoile les occurence d'un caractere dans un mot cache

        entree :
            mot_mystere : mot avec les caracteres en claires
            mot_affiche : mot avec les *
            lettre_choisie : lettre a devoiler dans le mot_affiche

        sorties :
            flag_trouve : boolean qui se met � 1 si on trouve au moins une occurence de la lettre


    **/
    int flag = 0;

    for(int i = 0; i< (int)mot_mystere.size();i++) {

        if(mot_mystere[i] == lettre_choisie){
            flag = 1;
            (*mot_affiche)[i]=lettre_choisie;

        }
    }

    return flag;
}

