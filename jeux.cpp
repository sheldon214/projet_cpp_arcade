#include "jeux.h"


Jeux::Jeux(string nom)
{
    this->nomjeu=nom;
    for (int i = 0 ; i< this->max_scores;i++){
        this->tabpseudo.push_back("-");
        this->tabscores.push_back(0);

    }
}

void Jeux::set_scores(int index, int score)
{
    this->tabscores[index] = score;
}

void Jeux::set_pseudo(int index, string name)
{
    this->tabpseudo[index] = name;
}



bool Jeux::AddScore(string pseudo, int nv_score)
{

    int i=0;
    bool flag = false;
    string tmp_pseudo;
    int taille = this->max_scores;

    for (i =0;i<taille;i++){

        if (nv_score > this->tabscores[i])
        {
                flag = true;
                int temp;

                for (int j = i; j< taille; j++){

                   temp = this->tabscores[j];
                   this->tabscores[j] = nv_score;
                   nv_score = temp;

                   tmp_pseudo = this->tabpseudo[j];
                   this->tabpseudo[j] = pseudo;
                   pseudo = tmp_pseudo;
               }

            break;
        }
    }

    return flag;



}



void Jeux::printab()
{
    cout <<"                 \tNEW Hight Score               \n\n"
         <<"\tRANG      |   \tNOM        |   \t\tSCORE\n"
         <<"--------------------------------------------------------"<< endl;

         for (int i =0;i<this->max_scores;i++){

            cout<<"\t"
                <<i+1
                <<"           \t"
                <<this->tabpseudo[i]
                <<"             \t"
                <<this->tabscores[i]<<endl;
//                cout << i+1 << " " << this->
         }
         cout <<"--------------------------------------------------------"<< endl;

}

void Jeux::menu(shared_ptr<Joueur> joueur)
{
    int choix_menu_jeu(0);
    bool return_(false);
    int score;
    do{

    cout << "--------------------- Bienvenue au jeu " << this->nomjeu << " ----------------" << endl;
    cout << "\t\t1. Jouer" << endl
         << "\t\t2. Regles" << endl
         << "\t\t3. Meilleurs Scores" << endl
         << "\t\t4.Menu Principal" << endl;

    cin >> choix_menu_jeu;

    switch(choix_menu_jeu){
        case 1 : {
          score = this->bouclePrincipale(joueur);

//            cout << "entrer score : " ;
//            cin >> score;

            joueur->addScore(this->getnom(),score);
            this->AddScore(joueur->getPseudo(),score);
        }
        break;
        case 2 : {
            this->regle();
        }
        break;
        case 3 : {
            this->printab();
        }
        break;
        default : {
            return_ = true;
        }
        break;
    }

    sleep(2);
    system("cls");
    }while(!return_);
}
