#ifndef ARCADE_H_INCLUDED
#define ARCADE_H_INCLUDED

#include "utile.h"
#include "joueur.h"
#include "pendu.h"
#include "mastermind.h"

class Arcade {

    private :
        vector <shared_ptr<Jeux>> ListJeux;
        shared_ptr<Joueur> joueur;
        map<string,string> options;
        string nom;


    public :
        // constructeur de l'arcade
        Arcade(string nom_,vector <string> list_nom_jeux);

        vector <shared_ptr<Jeux>> getJeux(){return this->ListJeux;}
        shared_ptr<Joueur> getJoueur(){return this->joueur;}
        map<string,string> getOptions(){return this->options;}

        void connexion();
        void MenuPrincipale();      // menu principale
//        void setOption();           // appliquer les options de l'utilisateur
        void savedata(string );     // sauvegarder les donnees du joueur
        void loaddataJeux(string ); // charger les donnees des jeux
        void loaddataJoueur(string); // charger les donnees des jeux
        void setcolor();


};





#endif // ARCADE_H_INCLUDED
