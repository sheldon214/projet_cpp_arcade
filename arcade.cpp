
#include "arcade.h"


Arcade::Arcade(string nom_,vector <string> list_nom_jeux)
{
    //
    this->nom = nom_;

    // creaation des objets jeux et rajout � la table des jeux de l'arcade
    for (auto nom_jeu : list_nom_jeux) {

                if(nom_jeu ==  "pendu" )this->ListJeux.push_back(make_shared <Pendu> (nom_jeu));
                else if(nom_jeu ==  "mastermind" )this->ListJeux.push_back(make_shared <Mastermind> (nom_jeu));

                // rajouter ici l'appel au constructeur pour un nouveau jeu

                else cout << nom_jeu << " n'est pas implementer --> ignorer " << endl;

        }
    }


void Arcade::connexion()
{
    string pseudo_joueur;
    cout << "entrer votre pseudo : " ;
    cin >> pseudo_joueur;

    bool present = rechercherJoueur(pseudo_joueur);
    this->joueur = make_shared <Joueur> (pseudo_joueur);

    if(!present){
        cout << "ce joueur n'existe pas --> creation d'un nouveau compte" << endl;
        ofstream fileuser("users.data",ios::app);
        fileuser<<pseudo_joueur << endl;
    }
    else {
        cout << "bienvenue " << pseudo_joueur << " --> chargement de vos donnees" << endl;
        this->loaddataJoueur("");
    }
}


void Arcade::MenuPrincipale()
{
      cout << "Bienvenue " << this->joueur->getPseudo() << " sur l'arcade " << this->nom << endl;
    cout << "Menu Principale : \n"
         << "\t\t1. Jeux\n"
         << "\t\t2. Info Joueur\n"
         << "\t\t3. Credits\n"
         << "\t\t4. Options\n"
         << "\t\t5. Quitter\n" << endl;
}


void Arcade::savedata(string source)
{

    // sauvegarde des donn�es des jeux

    for (auto jeu : this->ListJeux){
        cout << "-->saving data for " << jeu->getnom()  << "... " << endl;
        string pathfile  = source + jeu->getnom() + ".data" ;
        ofstream filejeux(pathfile);

        vector<int> tabscores = jeu->getTabScores();
        vector<string> tabpseudo = jeu ->getTabPseudo();

        for (int i = 0; i<tabscores.size();i++)  filejeux << tabpseudo[i] << " " << tabscores[i] << endl;

    }

    // sauvegarde des donnees du joueur


    shared_ptr<Joueur> perso = this->getJoueur() ;
    string pathfile2  = source + perso->getPseudo() + ".data";
    ofstream dataperso(pathfile2);

    cout << "-->saving data for " << perso->getPseudo()  << "... "<< endl;

    dataperso << perso->getPseudo()<<endl;
    dataperso << "option" ;

    map <string,string> :: iterator it;
    map <string,string> mapoption  = perso->getOptions();

    for (it=mapoption.begin();it!=mapoption.end();it++)
    {
        dataperso << " "
                  << it->first    // string (key)
                  << ' '
                  << it->second;   // string's value
    }
    dataperso << endl;

    map <string,vector<int>> :: iterator it2;
    map <string,vector<int>> scores  = perso->getScores();
    for (it2=scores.begin();it2!=scores.end();it2++)
    {

        dataperso << it2->first;   // string (key)

        for (auto j : it2->second) dataperso  << ' ' << j ;   // string's value
        dataperso << endl;

    }

}

void Arcade::loaddataJeux(string source)
{

    // chargement des donnees jeux

    for (auto jeu : this->ListJeux){
        cout << "-->loading data for " << jeu->getnom()  << "... " << endl;
        string pathfile  = source + jeu->getnom() + ".data" ;
        ifstream filejeux(pathfile);

        int scores ;
        string pseudo ;
        string line;
        int i(0);

        while(getline(filejeux,line)){
            stringstream streamline(line);
            streamline >> pseudo;
            streamline >> scores;
            jeu->set_scores(i,scores);
            jeu->set_pseudo(i,pseudo);
            i++;
        }

    }


}


void Arcade::loaddataJoueur(string source)
{

    shared_ptr<Joueur> perso = this->getJoueur() ;
    string pathfile2  = source + perso->getPseudo() + ".data";
    ifstream dataperso(pathfile2);

    cout << "-->loading data for " << perso->getPseudo()  << "... " << endl;

    string mot;
    string line;
    int value;

    getline(dataperso,mot);
    perso->setPseudo(mot);


    getline(dataperso,line);
    stringstream streamline(line);
    while(getline(streamline,mot,' ')){

        if(mot!="option") {
            string option_value;
            getline(streamline,option_value);
            perso->addOption(mot,option_value);
        }
    }

    while(getline(dataperso,line)){
        stringstream streamline(line);
        string nom_jeux;
        getline(streamline,nom_jeux,' ');
        while(getline(streamline,mot,' '))
        {
            const string mot_(mot);
            value = stoi(mot_);
            perso->addScore(nom_jeux,value);
        }

    }



}

void Arcade::setcolor()
{
    string color = this->getJoueur()->getOptions()["color"];
    if (color =="red") system("color 0C");
    else if(color == "green") system("color 0A");
}




