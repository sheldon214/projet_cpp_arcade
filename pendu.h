#ifndef PENDU_H_INCLUDED
#define PENDU_H_INCLUDED

#include "utile.h"
#include "jeux.h"

class Pendu:public Jeux {

    private  :

    public :

        int bouclePrincipale(shared_ptr<Joueur>joueur);
        Pendu(string nom):Jeux(nom){}
        void regle();

};

#endif // PENDU_H_INCLUDED
